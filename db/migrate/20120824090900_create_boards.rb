class CreateBoards < ActiveRecord::Migration
  def change
    create_table :boards do |t|
      t.integer :game_id
      t.integer :user_id
      t.text :board
      t.boolean :user_turn
      t.integer :turn

      t.timestamps
    end
  end
end
