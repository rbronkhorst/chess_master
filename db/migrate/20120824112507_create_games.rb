class CreateGames < ActiveRecord::Migration
  def change
    create_table :games do |t|
      t.string :user_name
      t.integer :turn
      t.string :slug

      t.timestamps
    end
  end
end
