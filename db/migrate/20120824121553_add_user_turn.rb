class AddUserTurn < ActiveRecord::Migration
  def up
    add_column :games, :user_turn, :boolean
  end

  def down
    remove_column :games, :user_turn
  end
end
