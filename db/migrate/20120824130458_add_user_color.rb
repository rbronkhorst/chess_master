class AddUserColor < ActiveRecord::Migration
  def up
     add_column :games, :user_color, :string
  end

  def down
     add_column :games, :user_color
  end
end
