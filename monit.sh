#!/bin/bash

if [[ -s "$HOME/.rvm/scripts/rvm" ]] ; then
  source "$HOME/.rvm/scripts/rvm"
elif [[ -s "/usr/local/rvm/scripts/rvm" ]] ; then
  source "/usr/local/rvm/scripts/rvm"
else
  printf "ERROR: An RVM installation was not found.\n"
fi

export RAILS_ENV=production
rvm use 1.9.2-p0
cd /sites/chess-master.kobie.nl/www/
rails runner DeepBlue.start &
echo $! > /tmp/chess_master.pid
