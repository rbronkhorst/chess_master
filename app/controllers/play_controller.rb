class PlayController < ApplicationController
  layout 'player'

  def welcome
    @game = Game.new
  end
  
  def game
    @game = Game.where(:slug => params[:slug]).first
    raise ActionController::RoutingError.new('Game not found') if @game.nil?
    @board = @game.boards.last
    @board.rotate if @game.user_color == 'black'
  end
  
  def move
    game = Game.where(:slug => params[:slug]).first
    begin
      raise "It's not your turn!" unless game.user_turn
      game.move(params[:from], params[:to])
      
    rescue MoveError
      message = $!.message
    else
      begin
        DeepBlue.queue_game(game.id)
      rescue
        message = "Move accepted, but i couldn't reach deep blue. Don't worry, he'll look at your game when he's back"
      else
        message = "Move accepted! Deep blue is thinking. Refresh the page to see the result."
      end
    end
    redirect_to short_game_path(:slug => game.slug), :notice => message
  end
  
  def register
    game = Game.new
    game.user_name = params[:game]
    
    if game.save
      unless game.user_turn
        begin
          DeepBlue.queue_game game.id 
        rescue
          message = "Game started. Deep blue could not be reached, but he'll look at it when he's back"
        else
          message = "Game started. Deep blue is thinking. Refresh the page to see the result."
        end
      end  
      redirect_to short_game_path(:slug => game.slug), :notice => message
    else
      redirect_to play_url, :notice => "Could not start game!"
    end
  end
end
