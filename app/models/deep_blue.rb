class DeepBlue
  URI="druby://localhost:53234"
  def initialize
    @queue = Queue.new
  end
  
  def self.start
    DeepBlue.new.run
  end
  
  def self.queue_game(game_id)
    blue_server = DRb::DRbObject.new_with_uri(URI)
    blue_server.queue_game game_id
  end
  
  def queue_game(game_id)
    @queue << game_id
  end
  
  def run
    # start up the DRb service
    DRb.start_service URI, self
    
    # let's see what there is to do
    look_for_work
    
    # We need the uri of the service to connect a client
    puts "Deep blue started on #{DRb.uri}"
    
    work
    
    # wait for the DRb service to finish before exiting
    DRb.thread.join
  end
  
  def look_for_work
    Game.where(:user_turn => false).each do |game|
      @queue.push game.id
    end
  end
  
  def work
    #all work and no play!
    while true
      game_id = @queue.pop
      puts "Working on game #{game_id}"
      Thread.pass
      begin
        Game.find(game_id).auto_move
      rescue
        puts $!.message
        puts $!.backtrace.join("\n")
      end
      puts "Finished with game #{game_id}, moving on!"
    end
  end
end
