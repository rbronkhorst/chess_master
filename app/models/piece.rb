class Piece
  TYPES = {
      'p' => :pawn,
      'k' => :king,
      'q' => :queen,
      'r' => :rook,
      'b' => :bishop,
      'n' => :knight
    }
  attr_reader :color, :type
  attr_accessor :field
  def initialize(color, type)
    raise "Invalid color" unless [:black, :white].include? color
    raise "Invalid piece type" unless TYPES.values.include? type
    @type = type
    @color = color
  end
  
  def move_to(new_field)
    @field.piece = nil unless @field.nil?
    
    @field = new_field
    @field.piece.field = nil unless @field.piece.nil?
    new_field.piece = self
  end
  
  def to_s
    color = (@color == :black) ? 'b' : 'w'
    type = TYPES.invert[@type]
    "#{color}#{type}"
  end
  
  def self.from_s(input)
    input.downcase!
    color = (input[0] == 'b') ? :black : :white
    type = TYPES[input[1]]
    Piece.new(color, type)
  end
  
  def check_field(row_offset, column_offset, valid_moves, options={})
    begin
      new_field = field.offset(row_offset, column_offset)
    rescue RangeError
      return false
    end
    return false if new_field.occupied? and Color.match(new_field.piece.color, @color)
    return false if options[:check_occupied] and new_field.free?
    return false if options[:check_free] and new_field.occupied?
    valid_moves << new_field
    new_field
  end
  
  def check_slide(row_offset, column_offset, valid_moves)
    current_row = 0
    current_column = 0
    sliding = true
    while sliding
      current_row += row_offset
      current_column += column_offset
      new_field = check_field(current_row, current_column, valid_moves)
      sliding = false if new_field == false or new_field.occupied?
    end
  end
  
  def can_move_to? (field)
    valid_moves.include? field
  end
  
  def valid_moves
    self.send("valid_#{@type}_moves")  
  end
  
  def play_direction
    result = 1
    result *= -1 if Color.match(@color, :black)
    result *= -1 if @field.board.is_rotated
    result
  end
  
  def valid_pawn_moves
    valid_moves = []
    # move forward
    forward_field = check_field(play_direction, 0, valid_moves, :check_free => true)
    
    #move to the side to capture an opponent
    check_field(play_direction, 1, valid_moves, :check_occupied => true)
    check_field(play_direction, -1, valid_moves, :check_occupied => true)
    
    start_row = (play_direction == 1) ? 1 : 6
    
    if @field.row == start_row and (forward_field != false) and forward_field.free?
      
      check_field(2 * play_direction, 0, valid_moves, :check_free => true)
    end
    valid_moves
  end
  
  def valid_rook_moves
    valid_moves = []
    check_slide(1,0, valid_moves)
    check_slide(0,1, valid_moves)
    check_slide(-1,0, valid_moves)
    check_slide(0,-1, valid_moves)
    valid_moves
  end
  
  def valid_bishop_moves
    valid_moves = []
    check_slide(1,1, valid_moves)
    check_slide(-1,1, valid_moves)
    check_slide(-1,-1, valid_moves)
    check_slide(1,-1, valid_moves)
    valid_moves
  end
  
  def valid_king_moves
    valid_moves = []
    check_field(1, -1, valid_moves)
    check_field(1, 0, valid_moves)
    check_field(1, 1, valid_moves)
    check_field(0, -1, valid_moves)
    check_field(0, 1, valid_moves)
    check_field(-1, -1, valid_moves)
    check_field(-1, 0, valid_moves)
    check_field(-1, 1, valid_moves)
    valid_moves
  end
  
  def valid_queen_moves
    valid_bishop_moves + valid_rook_moves
  end
  
  def valid_knight_moves
    valid_moves = []
    check_field(2, 1, valid_moves)
    check_field(2, -1, valid_moves)
    check_field(-2, 1, valid_moves)
    check_field(-2, -1, valid_moves)
    check_field(1, 2, valid_moves)
    check_field(1, -2, valid_moves)
    check_field(-1, 2, valid_moves)
    check_field(-1, -2, valid_moves)
    valid_moves
  end
  
  def value
    {:pawn => 1,
      :bishop => 3.5,
      :knight => 3.5,
      :rook => 5,
      :queen => 9,
      :king => 1000
      }[@type]
    
  end
end

