class Game < ActiveRecord::Base
  has_many :boards, :dependent => :delete_all
  before_create :generate_slug
  before_create :set_turn
  before_create :create_board
  validates_presence_of :user_name
  
  def reverse_color(color)
    (color.downcase == 'black') ? 'white' : 'black'
  end
  def computer_color
    reverse_color(self.user_color)
  end
  
  def set_turn
    self.user_color = (rand(2) == 0)? 'black' : 'white' if self.user_color.blank?
    self.user_turn = (self.user_color == 'white')? true : false
    self.turn = 0 if self.turn.blank?
  end
  
  def board
    boards.last
  end
  
  def current_color
    (user_turn) ? user_color : Color.reverse(user_color)
  end
  
  def get_coordinates(position)
    begin
      row = Integer(position[1..1]) - 1
    rescue ArgumentError
      raise MoveError, "Invalid position row: '#{position}'"
    end
    raise MoveError, "Invalid position row: '#{position}'" unless row >= 0 and row < Board::BOARD_WIDTH
    column = position[0..0].ord - 'a'.ord
    raise MoveError, "Invalid position column: '#{position}'" unless column >= 0 and column < Board::BOARD_WIDTH
    [row, column]
  end
  
  def move(from, to, override_checks=false)
    color = current_color
    raise MoveError, "Please fill in two board positions like 'a3' and 'h4'" unless from.size == 2 and to.size == 2
    board = boards.last.dup
    board.turn += 1
    from_row, from_column = get_coordinates(from)
    to_row, to_column = get_coordinates(to)
    board.move(from_row, from_column, to_row, to_column, color, override_checks)
    self.user_turn = (self.user_turn) ? false : true
    self.turn = board.turn
    boards << board
    save
  end
  
  def print_moves(position)
    row, column = get_coordinates(position)
    board.print_moves(row, column)
  end
  
  def pretty_print
    puts "Turn for: #{current_color.to_s.capitalize}"
    board.pretty_print
  end
  
  alias :pp :pretty_print
  
  def auto_move(override_checks=false)
    raise "Can not automove if it's the users turn!" if user_turn and not override_checks
    moves = []
    pieces = board.pieces[current_color.to_sym]
    
    
    pieces.each do |piece|
      moves += piece.valid_moves.map{|f| [piece, f]}
    end
    
    max_hit = 0
    chosen_move = nil
    moves.each do |move|
      piece_value = move[1].piece.value rescue 0
      if piece_value > max_hit
        max_hit = piece_value
        chosen_move = move
      end
    end
    chosen_move = moves[rand(moves.size)] if chosen_move.nil?
        
    piece = chosen_move[0]
    new_field = chosen_move[1]
    old_field = piece.field
    puts "Moving my #{piece.type.capitalize} from #{old_field.name} to #{new_field.name}"
    move(old_field.name, new_field.name)
    self
  end
  
  def create_board
    first_board = Board.start_positions
    first_board.user_turn = self.user_turn
    first_board.turn = 0
    self.boards << first_board
  end
  
  def generate_slug
    self.slug = Digest::MD5::hexdigest("Chess$%^&Master#{rand}") if self.slug.blank?
  end
end
