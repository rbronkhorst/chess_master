class Color
  def self.black
    :black
  end
  
  def self.white
    :white
  end
  
  def self.match(color1, color2)
    color1.to_s.downcase == color2.to_s.downcase
  end
  
  def self.black?(color)
    match(color, :black)
  end
  
  def self.white(color)
    match(color, :white)
  end
  
  def self.reverse(color)
    (match(color, :black)) ? :white : :black
  end
end
