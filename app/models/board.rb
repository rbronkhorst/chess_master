class Board < ActiveRecord::Base
  BOARD_WIDTH = 8
  BOARD_HEIGHT = 8

  belongs_to :game
  validates_presence_of :turn
  before_save :check_rotation
  before_save :serialize_fields
  after_initialize :initialize_fields
  attr_reader :fields, :is_rotated, :pieces
  def initialize_fields
    @is_rotated = false
    empty_fields
    unless self.board.blank?
      load_fields
    end
  end
  
  def get_field(row, column)
    raise RangeError, "Offset out of bounds" if row >= Board::BOARD_HEIGHT or row < 0
    raise RangeError, "Offset out of bounds" if column >= Board::BOARD_WIDTH or column < 0
    @fields[row][column]
  end
  
  
  def dup
    serialize_fields
    result = super
    result.initialize_fields
    result
  end
  
  def rotate
    @fields.reverse!
    @fields.each_with_index do |row, row_index|
      row.reverse!
      row.each_with_index do |field, column_index|
        field.column = column_index
        field.row = row_index
      end
    end
    @is_rotated = (@is_rotated)? false : true
  end

  # if the board is rotated, rotate it back before saving so that we always
  # save the board with white on the bottom
  def check_rotation
    rotate if @is_rotated
  end
  
  def empty_fields
    color = :black
    @fields = []
    (0..(BOARD_HEIGHT - 1)).each do |row|
      @fields[row] = []
      (0..(BOARD_WIDTH - 1)).each do |column|
        color = (color == :black)? :white : :black unless column == 0
        @fields[row][column] = Field.new(color, row, column, self)
      end
    end
  end
  
  def piece= (new_piece)
    new_piece.field = self unless new_piece.nil?
    super(new_piece)
  end
  
  def load_fields
    json_board = JSON.parse(self.board)
    color = :black
    @pieces = {:black => [], :white => []}
    json_board.each_with_index do |row, row_index|
      row.each_with_index do |json_field, column_index|
        field = @fields[row_index][column_index]
        unless json_field.blank?
          piece =  Piece.from_s(json_field)
          piece.move_to field
          @pieces[piece.color.to_sym] << piece
        end
      end
    end
  end

  def self.start_positions
    new_board = Board.new
    start_positions = [
      %w{wr wn wb wq wk wb wn wr},
      %w{wp wp wp wp wp wp wp wp},
      Array.new(6),
      Array.new(6),
      Array.new(6),
      Array.new(6),
      %w{bp bp bp bp bp bp bp bp},
      %w{br bn bb bq bk bb bn br},
     ].to_json
    new_board.board = start_positions
    new_board.load_fields
    new_board
  end
  
  def serialize_fields
    self.board = to_json
    true
  end
  
  def pretty_print(xmarks=[])
    puts "  a  b  c  d  e  f  g  h"
    raise "Can't print if the board is rotated" if @is_rotated
    @fields.reverse.each_with_index do |row, row_index|
      $stdout.print "#{8 - row_index} "
      row.each do |field|
        field_caption = (field.to_s.nil?) ? '..' : field.to_s
        field_caption[1]='X' if xmarks.include? field
        $stdout.print "#{field_caption} "
      end
      puts
    end
    ""
  end
  
  alias :pp :pretty_print
  
  def print_moves(row, column)
    field = get_field(row, column)
    raise MoveError, "No piece at this field!" if field.piece.nil?
    valid_moves = field.piece.valid_moves
    pretty_print(valid_moves)
  end
  
  def to_json
    result = []
    @fields.each_with_index do |row, row_index|
      result[row_index] = []
      row.each_with_index do |field, column_index|
        result[row_index][column_index] = field.to_s  
      end
    end
    result.to_json
  end
  
  def move(from_row,from_column, to_row, to_column, color, skip_checks=false)
    from = fields[from_row][from_column]
    to = fields[to_row][to_column]
    if from.free? or not Color.match(color, from.piece.color)
      raise MoveError, "You don't have a piece at #{from.name}!" unless skip_checks
    end
    if to.occupied? and Color.match(color, to.piece.color)
      raise MoveError, "Tile #{to.name} is already occupied by your #{to.piece.type.capitalize}!" unless skip_checks
    end
    unless from.piece.can_move_to? to
      raise MoveError, "Not a valid move for your #{from.piece.type.capitalize}!" unless skip_checks
    end
    from.piece.move_to to
    self
  end
  
end

