class Field
  attr_accessor :piece, :color, :row, :column,:board, :name

  def initialize(color, row, column, board)
    @color = color
    @row = row
    @column = column
    @board = board
    @name = "#{('a'.ord + @column).chr}#{@row + 1}"
  end
  
  def dup
    result = super
    result.piece = self.piece.dup
    result
  end

  def offset(rows, columns)
    new_row = rows + @row
    new_column = columns + @column
    @board.get_field(new_row, new_column)
  end
  
  
  def to_s
    return nil if @piece.nil?
    @piece.to_s
  end
  
  def free?
    @piece == nil
  end
  
  def occupied?
    not free?
  end
  
  def black?
    @color == :black
  end
  
  def white?
    @color == :white
  end 
end

